# Battery

A library for reading the battery status on Unix machines.  Based on
[battery](https://github.com/distatus/battery) by Karol Woźniak.

battery [![Build Status](https://gitlab.com/lyda/battery/actions/workflows/tests.yml/badge.svg?branch=master)](https://gitlab.com/lyda/battery/actions/workflows/tests.yml) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/lyda/battery)](https://goreportcard.com/report/gitlab.com/lyda/battery) [![GoDoc](https://pkg.go.dev/badge/gitlab.com/lyda/battery)](https://pkg.go.dev/gitlab.com/lyda/battery)
=======

Cross-platform, normalized battery information library.

Gives access to a system independent, typed battery state, capacity, charge and voltage values recalculated as necessary to be returned in mW, mWh or V units.

Currently supported systems:

* Linux 2.6.39+
* OS X 10.10+
* Windows XP+
* FreeBSD
* DragonFlyBSD
* NetBSD
* OpenBSD
* Solaris

Installation
------------

```bash
$ go get -u gitlab.com/lyda/battery
```

Code Example
------------

```go
package main

import (
	"fmt"

	"gitlab.com/lyda/battery"
)

func main() {
	batteries, err := battery.GetAll()
	if err != nil {
		fmt.Println("Could not get battery info!")
		return
	}
	for i, battery := range batteries {
		fmt.Printf("Bat%d: ", i)
		fmt.Printf("state: %s, ", battery.State.String())
		fmt.Printf("current capacity: %f mWh, ", battery.Current)
		fmt.Printf("last full capacity: %f mWh, ", battery.Full)
		fmt.Printf("design capacity: %f mWh, ", battery.Design)
		fmt.Printf("charge rate: %f mW, ", battery.ChargeRate)
		fmt.Printf("voltage: %f V, ", battery.Voltage)
		fmt.Printf("design voltage: %f V\n", battery.DesignVoltage)
	}
}
```

CLI
---

There is also a little utility which - more or less - mimicks the GNU/Linux `acpi -b` command.

*Installation*

```bash
$ go install gitlab.com/lyda/battery/cmd/battery@latest
```

*Usage*

```bash
$ battery
BAT0: Full, 95.61% [Voltage: 12.15V (design: 12.15V)]
```
